from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoListForm


def todolist_list(request):
    todolist_list = TodoList.objects.all()

    context = {"todolist_list": todolist_list}

    return render(request, "todos/todolist.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)

    context = {"todo_list": todo_list}

    return render(request, "todos/detail.html", context)


def create_todo(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)

        if form.is_valid():
            list = form.save()

            return redirect("todo_list_detail", id=list.id)

    else:
        form = TodoListForm()

    context = {"create_form": form}
    return render(request, "todos/create.html", context)


def update_todo(request, id):
    todolist = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)

        if form.is_valid():
            form.save()

            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=todolist)

    context = {"form": form}

    return render(request, "todos/update.html", context)


def delete_todo(request, id):
    todolist = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        todolist.delete()

        return redirect("todo_list_list")

    return render(request, "todos/delete.html")
