from django.urls import path
from todos.views import (
    todolist_list,
    todo_list_detail,
    create_todo,
    update_todo,
    delete_todo,
)

urlpatterns = [
    path("", todolist_list, name="todo_list_list"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("create/", create_todo, name="todo_list_create"),
    path("<int:id>/edit/", update_todo, name="todo_list_update"),
    path("<int:id>/delete/", delete_todo, name="todo_list_delete"),
]
